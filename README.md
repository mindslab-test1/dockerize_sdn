uploaded by mjchae

# SDN 이미지 제작 방법
## SDN 빌드 참고
SDN source readme를 참고해서 SDN 도커 내에서 build 절자를 ansible playbook 파일(sdn_install.yaml)에 생성한다.
SDN 도커는 이미 격리된 환경이므로 virtualenv 연관성은 제거한다. 참고로 도커파일에 virtualenv 패키지는 설치하게 함.
```
#----- Dockerfile에서 생성되도록 함
cd aicc/src/sdn
sudo yum install -y python-devel
sudo yum install -y epel-release
sudo yum install -y libsamplerate-devel libsamplerate
sudo yum install -y python-pip
sudo pip install --upgrade pip
sudo pip install virtualenv
virtualenv -p python2 venv --> 삭제
source venv/bin/activate --> 삭제
export PYTHONPATH=$MAUM_ROOT/lib/python
#----- Docker 컨테이너 내에서 git clone함
git clone https://github.com/mindslab-ai/aicc.git
#----- build 관련은 ansible playbook(sdn_install.yaml)로 실행되게 함
pip install -r requirements.txt --> 변경(일부 package가 버전이 차이가 있어 수정 필요. ansible files 경로에 새 requirements.txt가 존재)
cd ../..
git submodule init
git submodule update src/sdn/pysoundtouch
cd src/sdn/pysoundtouch
cd soundtouch
./bootstrap
./configure --enable-integer-samples CXXFLAGS="-fPIC"
make
sudo make install
cd ..
python setup.py install
cd ..
make install
```

## SDN docker 이미지 만들기 
### Dockerfile 만들기
Dockerfile을 만든다. 현재 경로에 있는 Dockerfile을 참고

### Docker image build
```
docker build --tag maum-sdn:1.0.3 .
```
### Docker container run
hostname을 localhost로 함
```
docker run -it --hostname localhost -p 31050:31050 --name maum-sdn-1.0.3 -v /fsapplog:/fsapplog maum-sdn:1.0.3 /bin/bash
```
### git clone AICC repo
$MAUM_ROOT로 이동하여 aicc를 git clone한다
```
git clone https://github.com/mindslab-ai/aicc.git aicc
```
### Ansible host add
ansible hosts 파일을 열어서 마지막 line에 localhost를 추가한다. 이 작업을 하지 않더라도 local실행은 가능(warning 메시지 출력).
```
vim /etc/ansible/hosts
~~~~
localhost
```

### Run ansible playbook 실행
반드시 아래의 순서대로 실행하여야 한다. 
+ 첫 번째 sdn_playbook2.yaml은 빌드에 필요한 환경들을 미리 작업  
+ 실제 build는 sdn_install.yaml에서 진행된다.
```
ansible-playbook -vv sdn_playbook2.yaml
ansible-playbook -vv sdn_install.yaml 
```
모두 정상적으로 실행되면 도커 컨테이너를 빠져 나와서 새 이미지를 제작한다.

### Docker container commit
현재까지 수정된 container를 다시 이미지로 저장. 이미지의 tag를 1.0.3이라고 가정
```
docker commit maum-sdn-1.0.0 docker.maum.ai:443/maum-sdn:1.0.0 
```
### Docker 실행 및 service 실행 
만약 LB 설정을 위해 VIP를 사용하는 경우 해당 서비스 포트(여기서는 50051)가 이미 LB에 의해 bind된 상태라면 'Address already in use' 같은 에러가 발생할 수 있는데 이런 경우 -p 옵션에 VIP 주소가 아닌 로컬 주소를 bind(-p 10.122.64.95:50051)해서 error를 피할 필요가 있다.
```
docker run -it --hostname localhost -p 10.122.64.95:50051:50051 --name maum-sdn-1.0.3 -v /fsapplog:/fsapplog docker.maum.ai:443/maum-sdn:1.0.3 /bin/bash
(or)
docker run -itd --hostname localhost -p 31050:31050 --name maum-sdn-1.0.3 -v /fsapplog:/fsapplog docker.maum.ai:443/maum-sdn:1.0.3
docker exec maum-sdn-1.0.0 svd
```

### github에 repository 생성
```
echo "# dockerize_sdn" >> README.md
git init
git add README.md
git commit -m "first commit"
git branch -M master
git remote add origin https://github.com/shinwc926/dockerize_sdn.git
git push -u origin master
```
