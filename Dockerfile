FROM centos:7
MAINTAINER Wonchang Shin shinwc@mindslab.ai

RUN yum -y update && yum -y install epel-release\
    && yum -y install python-devel \
    && yum install -y libsamplerate-devel libsamplerate \
    && yum install -y python-pip \
    && pip install --upgrade pip \
    && pip install virtualenv \
    && yum -y groupinstall 'Development Tools' \
    && yum -y install golang \
    && yum -y install ansible \
    && yum -y install httpie \
    && yum -y install vim

WORKDIR /srv/maum

ADD sdn_playbook2.yaml /srv/maum/ansible/sdn_playbook2.yaml
ADD sdn_install.yaml /srv/maum/ansible/sdn_install.yaml
ADD tts-load.tar /srv/maum/
COPY ./files /srv/maum/ansible/files/
COPY ghz.tar.gz /srv/maum/ansible/
COPY ghz-web.tar.gz /srv/maum/ansible/
COPY Dockerfile /srv/maum/ansible
COPY README.md /srv/maum/ansible

RUN mkdir -p /srv/maum/sdn/cache
RUN mkdir -p /fsapplog/sdn

ENV MAUM_ROOT=/srv/maum
ENV PATH=/srv/maum/bin:$PATH
ENV PYTHONPATH=/srv/maum/lib/python
ENV TZ=Asia/Seoul
RUN localedef -f UTF-8 -i ko_KR ko_KR.utf8
ENV LC_ALL=ko_KR.UTF-8
ENV USER=root

EXPOSE 31050
EXPOSE 50051

VOLUME /fsapplog

